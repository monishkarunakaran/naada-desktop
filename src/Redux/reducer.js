const initialState = {
  dummy: "Not yet changed",
};

const reducer = (state = initialState, action) => {
  const newState = { ...state };

  switch (action.type) {
    case "DUMMY_ACTION":
      newState.dummy = action.value;
      break;
  }
  return newState;
};

export default reducer;
