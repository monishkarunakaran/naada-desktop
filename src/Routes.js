import React, { lazy, Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useRouteMatch,
  Link,
} from "react-router-dom";
import routePath from "./constants/routes.json";

const Home = lazy(() => import("./components/home.jsx"));
const Artist = lazy(() => import("./components/artist.jsx"));
export default function Routes() {
  let match = useRouteMatch();
  console.log(match);
  return (
    <Suspense fallback={"loading..."}>
      <div>
        <Router>
          <nav>
            <ul>
              <li>
                <Link to={routePath.HOME}>Home</Link>
              </li>
              <li>
                <Link to={routePath.ARTIST}>Artist</Link>
              </li>
            </ul>
          </nav>
          <Switch>
            <Route path={routePath.ARTIST} component={Artist} />
            <Route path={routePath.HOME} component={Home} />
          </Switch>
        </Router>
      </div>
    </Suspense>
  );
}
