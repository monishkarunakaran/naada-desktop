import "../assets/css/App.css";
import React, { Component } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import Routes from "../Routes";
import routePath from "../constants/routes.json";
import { Provider } from "react-redux";
import { store, persistor } from "../Redux/store";
import { PersistGate } from "redux-persist/integration/react";
class App extends Component {
  render() {
    console.log(persistor);
    return (
      <Provider store={store}>
        <PersistGate loading={false} persistor={persistor}>
          <h1>Naada</h1>
          <Router>
            <Routes />
          </Router>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
